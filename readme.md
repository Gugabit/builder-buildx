# Docker image with buildx

## Gitlab-CI Usage

### The folowing example is used to build for arm64 using this image on gitlab-ci

```yaml gitlab-ci.yaml
# gitlab-ci.yml
# Multi-arch build for gitlab-ci

stages:
  - build

build:
  stage: build
  image: gugabit/buildx:stable
  variables:
    DOCKER_HOST: tcp://docker:2375
  services:
    - docker:dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker buildx create --use
    # - docker run --rm --privileged tonistiigi/binfmt --install arm64
  script:
    - docker pull $CI_REGISTRY_IMAGE:latest || true
    - docker buildx build --cache-from $CI_REGISTRY_IMAGE:latest --tag $CI_REGISTRY_IMAGE:latest --platform linux/amd64,linux/arm64 --push .
  only:
    - main
```

## Command line usage

```sh
docker run -it --rm gugabit/buildx:latest docker buildx ...
```

## Build from dockerfile

```sh
docker build -t gugabit/buildx:stable .
```
